module gitlab.com/rizkyekoputra/simple-server

require (
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.4
	github.com/lib/pq v1.3.0
	github.com/subosito/gotenv v1.2.0
	golang.org/x/perf v0.0.0-20200318175901-9c9101da8316 // indirect
	gopkg.in/oauth2.v3 v3.12.0
)

go 1.13
