package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"

	"gitlab.com/rizkyekoputra/simple-server/controllers"

	"github.com/gorilla/mux"
	"github.com/subosito/gotenv"
	"gitlab.com/rizkyekoputra/simple-server/driver"
)

var db *sql.DB

func init() {
	gotenv.Load()
}

func logFatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	db = driver.ConnectDB()
	controllers := controllers.Controllers{}

	router := mux.NewRouter()

	router.HandleFunc("/books", controllers.GetBooks(db)).Methods("GET")
	router.HandleFunc("/books/{id}", controllers.GetBook(db)).Methods("GET")
	router.HandleFunc("/books", controllers.AddBook(db)).Methods("POST")
	router.HandleFunc("/books", controllers.UpdateBook(db)).Methods("PUT")
	router.HandleFunc("/books/{id}", controllers.RemoveBook(db)).Methods("DELETE")

	fmt.Println("Server is running at port 8080")
	log.Fatal(http.ListenAndServe(":8080", router))
}
